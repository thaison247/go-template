package jwt

import (
	"gitlab.com/LamTNB/go-template/pkg/constant"
	"gitlab.com/LamTNB/go-template/pkg/util"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

// JWT is jwt middleware
func JWT() gin.HandlerFunc {
	return func(c *gin.Context) {
		var code int
		var data interface{}

		code = constant.SUCCESS
		token := c.Query("Authorization")
		if token == "" {
			code = constant.INVALID_PARAMS
		} else {
			_, err := util.ParseToken(token)
			if err != nil {
				switch err.(*jwt.ValidationError).Errors {
				case jwt.ValidationErrorExpired:
					code = constant.ERROR_AUTH_CHECK_TOKEN_TIMEOUT
				default:
					code = constant.ERROR_AUTH_CHECK_TOKEN_FAIL
				}
			}
		}

		if code != constant.SUCCESS {
			c.JSON(http.StatusUnauthorized, gin.H{
				"code": code,
				"msg":  constant.GetMsg(code),
				"data": data,
			})

			c.Abort()
			return
		}

		c.Next()
	}
}
