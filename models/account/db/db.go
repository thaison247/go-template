package account_db

import (
	"context"
	"errors"
	"gitlab.com/LamTNB/go-template/models"
	"gitlab.com/LamTNB/go-template/models/account"
	"gorm.io/gorm"
)

var accountRepository Repository

type Repository struct {
	models.Client
}

func Setup(client models.Client) {
	accountRepository = Repository{
		Client: client,
	}
}

func GetRepository() account.Repository {
	return accountRepository
}

func (accountRepository Repository) FindOne(id uint, context context.Context) (account.Account, error) {
	var acc account.Account
	var err error
	db := accountRepository.GetTransaction(context).First(&acc, id)
	if errors.Is(db.Error, gorm.ErrRecordNotFound) {
		err = account.ErrAccountNotFound
	}
	return acc, err
}

func (accountRepository Repository) Find(context context.Context) ([]account.Account, error) {
	var acc []account.Account
	db := accountRepository.GetTransaction(context).Table(account.Account{}.TableName()).Find(&acc)
	return acc, db.Error
}

func (accountRepository Repository) Save(acc *account.Account, context context.Context) (*account.Account, error) {
	db := accountRepository.GetTransaction(context).Save(acc)
	return acc, db.Error
}

func (accountRepository Repository) Update(acc *account.Account, context context.Context) (*account.Account, error) {
	db := accountRepository.GetTransaction(context).Save(acc)
	return acc, db.Error
}
func (accountRepository Repository) Delete(acc uint, context context.Context) (bool, error) {
	db := accountRepository.GetTransaction(context).Delete(acc)
	return db.RowsAffected > 0, db.Error
}

func (accountRepository Repository) Count(ctx context.Context) (int64, error) {
	var total int64
	db := accountRepository.GetTransaction(ctx).Table(account.Account{}.TableName()).Count(&total)
	return total, db.Error
}
