package account

import "gorm.io/gorm"

type Account struct {
	gorm.Model
	Name string `gorm:"name"`
}

func (Account) TableName() string {
	return "account"
}
