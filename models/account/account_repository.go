package account

import (
	"context"
	"errors"
)

var (
	ErrAccountNotFound = errors.New("the account not found")
)

type Repository interface {
	FindOne(id uint, context context.Context) (Account, error)
	Find(context context.Context) ([]Account, error)
	Save(account *Account, context context.Context) (*Account, error)
	Update(account *Account, context context.Context) (*Account, error)
	Delete(id uint, context context.Context) (bool, error)
	Count(ctx context.Context) (int64, error)
}
