package models

import (
	"context"
	"fmt"
	"gitlab.com/LamTNB/go-template/pkg/setting"
	"gorm.io/driver/postgres"
	_ "gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"sync"
)

var once sync.Once
var db *gorm.DB
var client Client

// Setup initializes the database instance
func Setup() {
	once.Do(func() {
		var err error
		dsn := fmt.Sprintf("host=%v user=%v password=%v dbname=%v port=%v sslmode=disable TimeZone=%v", setting.DatabaseSetting.Host,
			setting.DatabaseSetting.User, setting.DatabaseSetting.Password,
			setting.DatabaseSetting.Name, setting.DatabaseSetting.Port,
			setting.DatabaseSetting.Timezone)
		db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
		// Get generic database object sql.DB to use its functions
		if err != nil {
			log.Fatalf("models.Setup err: %v", err)
		}
		client = Client{db: db}
	})

}

type Client struct {
	db *gorm.DB
}

func GetClient() Client {
	return client
}

func (c Client) StartTransaction(ctx context.Context) context.Context {
	return context.WithValue(ctx, "db_tx", c.db.Begin())
}

func (c Client) CommitTransaction(ctx context.Context) {
	ctx.Value("db_tx").(*gorm.DB).Commit()
}

func (c Client) RollbackTransaction(ctx context.Context) {
	ctx.Value("db_tx").(*gorm.DB).Rollback()
}

func (c Client) GetTransaction(ctx context.Context) *gorm.DB {
	tx := ctx.Value("db_tx")
	if tx != nil {
		return tx.(*gorm.DB)
	} else {
		return c.db
	}
}
