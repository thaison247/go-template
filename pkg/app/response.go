package app

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/LamTNB/go-template/pkg/constant"
	"net/http"
)

type Gin struct {
	C *gin.Context
}

type Response struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

// Response setting gin.JSON
func (g *Gin) Response(errCode int, data interface{}) {
	g.C.JSON(http.StatusOK, Response{
		Code: errCode,
		Msg:  constant.GetMsg(errCode),
		Data: data,
	})
	return
}
