package util

import (
	"github.com/gin-gonic/gin"
	"strconv"
)

type FetchType int

const (
	FetchAll FetchType = iota
	Paging
)

type PageRequest struct {
	Type FetchType
	Page int
	Size int
}

// GetPageRequest get page parameters
func GetPageRequest(c *gin.Context) PageRequest {
	fetchTypeVal, _ := strconv.Atoi(c.Query("type"))
	if FetchAll == FetchType(fetchTypeVal) {
		return PageRequest{FetchAll, 0, 0}
	}
	page, _ := strconv.Atoi(c.Query("page"))
	size, _ := strconv.Atoi(c.Query("size"))
	if page > 0 {
		page = (page - 1) * size
	}

	return PageRequest{Paging, page, size}
}
