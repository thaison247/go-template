package setting

import (
	"time"

	"github.com/go-ini/ini"
)

type App struct {
	JwtSecret       string
	PrefixUrl       string
	LogSavePath     string
	LogSaveName     string
	LogFileExt      string
	TimeFormat      string
	RuntimeRootPath string
}

var AppSetting = &App{}

type Server struct {
	RunMode      string
	HttpPort     int
	ReadTimeout  time.Duration
	WriteTimeout time.Duration
}

var ServerSetting = &Server{}

type Database struct {
	Type        string
	User        string
	Password    string
	Host        string
	Port        string
	Timezone    string
	Name        string
	TablePrefix string
}

var DatabaseSetting = &Database{}

type Redis struct {
	Host        string
	Password    string
	MaxIdle     int
	MaxActive   int
	IdleTimeout time.Duration
}

var RedisSetting = &Redis{}

var cfg *ini.File

// Setup initialize the configuration instance
func Setup() {
	initAppConfig()

	initAppSetting()
	initServerSetting()
	initDatabaseSetting()
	initRedisSetting()
}

func initAppSetting() {
	AppSetting.JwtSecret = AppConfig.Conf.GetString("app.jwt_secret", "unknow")
	AppSetting.PrefixUrl = AppConfig.Conf.GetString("app.prefix_url", "unknow")
	AppSetting.LogSavePath = AppConfig.Conf.GetString("app.log.path", "unknow")
	AppSetting.LogSaveName = AppConfig.Conf.GetString("app.log.name", "unknow")
	AppSetting.RuntimeRootPath = AppConfig.Conf.GetString("app.log.root_path", "")
	AppSetting.LogFileExt = AppConfig.Conf.GetString("app.log.ext", "unknow")
	AppSetting.TimeFormat = AppConfig.Conf.GetString("app.log.time_format", "unknow")

}

func initServerSetting() {
	ServerSetting.HttpPort = int(AppConfig.Conf.GetInt32("app.server.port", 8000))
	ServerSetting.RunMode = AppConfig.Conf.GetString("app.server.mode", "release")
	ServerSetting.WriteTimeout = AppConfig.Conf.GetTimeDuration("app.server.write_time_out", time.Second)
	ServerSetting.ReadTimeout = AppConfig.Conf.GetTimeDuration("app.server.read_time_out", time.Second)
}

func initRedisSetting() {
	RedisSetting.Host = AppConfig.Conf.GetString("redis.host", "localhost:6379")
	RedisSetting.MaxActive = int(AppConfig.Conf.GetInt32("redis.max_active", 30))
	RedisSetting.MaxIdle = int(AppConfig.Conf.GetInt32("redis.max_idle", 30))
	RedisSetting.IdleTimeout = AppConfig.Conf.GetTimeDuration("redis.idle_timeout", time.Second)
	RedisSetting.Password = AppConfig.Conf.GetString("redis.password", "password")
}

func initDatabaseSetting() {
	DatabaseSetting.Type = AppConfig.Conf.GetString("database.type", "postgres")
	DatabaseSetting.Host = AppConfig.Conf.GetString("database.host", "127.0.0.1")
	DatabaseSetting.Port = AppConfig.Conf.GetString("database.port", "5432")
	DatabaseSetting.Name = AppConfig.Conf.GetString("database.name", "go_template")
	DatabaseSetting.User = AppConfig.Conf.GetString("database.username", "postgres")
	DatabaseSetting.Password = AppConfig.Conf.GetString("database.password", "123456")
	DatabaseSetting.Timezone = AppConfig.Conf.GetString("database.timezone", "UTC")

}
