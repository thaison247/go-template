package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/LamTNB/go-template/models"
	"gitlab.com/LamTNB/go-template/models/account/db"
	"gitlab.com/LamTNB/go-template/pkg/logging"
	"gitlab.com/LamTNB/go-template/pkg/setting"
	"gitlab.com/LamTNB/go-template/pkg/util"
	"gitlab.com/LamTNB/go-template/routers"
	"log"
	"net/http"
)

func init() {
	setting.Setup()
	models.Setup()
	logging.Setup()
	//gredis.Setup()
	util.Setup()

	//load repository
	client := models.GetClient()
	account_db.Setup(client)
}

func main() {
	gin.SetMode(setting.ServerSetting.RunMode)
	routersInit := routers.InitRouter()
	readTimeout := setting.ServerSetting.ReadTimeout
	writeTimeout := setting.ServerSetting.WriteTimeout
	endPoint := fmt.Sprintf(":%d", setting.ServerSetting.HttpPort)
	maxHeaderBytes := 1 << 20

	server := &http.Server{
		Addr:           endPoint,
		Handler:        routersInit,
		ReadTimeout:    readTimeout,
		WriteTimeout:   writeTimeout,
		MaxHeaderBytes: maxHeaderBytes,
	}

	log.Printf("[info] start http server listening %s", endPoint)

	server.ListenAndServe()
}
