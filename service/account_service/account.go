package account_service

import (
	"context"
	"gitlab.com/LamTNB/go-template/models/account"
	"gitlab.com/LamTNB/go-template/models/account/db"
	"gitlab.com/LamTNB/go-template/pkg/util"
	"gorm.io/gorm"
)

type Account struct {
	ID          uint   `json:"id"`
	Name        string `json:"name"`
	PageRequest util.PageRequest
}

func (a *Account) Create(context context.Context) error {
	repository := account_db.GetRepository()
	if _, err := repository.Save(&account.Account{
		Name: a.Name,
	}, context); err != nil {
		return err
	}
	return nil
}

func (a *Account) Update(ctx context.Context) error {
	repository := account_db.GetRepository()
	if _, err := repository.Update(&account.Account{
		Model: gorm.Model{
			ID: a.ID,
		},
		Name: a.Name,
	}, ctx); err != nil {
		return err
	}
	return nil
}

func (a *Account) FindOne(ctx context.Context) error {
	repository := account_db.GetRepository()
	accountModel, err := repository.FindOne(a.ID, ctx)
	if err != nil {
		return err
	}
	a.ID = accountModel.ID
	a.Name = accountModel.Name
	return nil
}

func (a *Account) Find(ctx context.Context) ([]account.Account, error) {
	repository := account_db.GetRepository()
	return repository.Find(ctx)
}

func (a *Account) Delete(ctx context.Context) error {
	repository := account_db.GetRepository()
	_, err := repository.Delete(a.ID, ctx)
	return err
}

func (a *Account) Count(ctx context.Context) (int64, error) {
	repository := account_db.GetRepository()
	return repository.Count(ctx)
}
