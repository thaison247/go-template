package v1

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/LamTNB/go-template/pkg/app"
	"gitlab.com/LamTNB/go-template/pkg/constant"
	"gitlab.com/LamTNB/go-template/pkg/util"
	"gitlab.com/LamTNB/go-template/service/account_service"
)

func SayHello(c *gin.Context) {
	appG := app.Gin{C: c}

	data := make(map[string]interface{})
	data["say"] = "Hello world"
	appG.Response(constant.SUCCESS, data)
}

func GetAccounts(c *gin.Context) {
	appG := app.Gin{C: c}
	accountService := account_service.Account{
		PageRequest: util.GetPageRequest(c),
	}
	data := make(map[string]interface{})
	accounts, err := accountService.Find(c)
	if err != nil {
		appG.Response(constant.ERROR_GET_ACCOUNT_FAIL, nil)
		return
	}
	data["record"] = accounts

	if accountService.PageRequest.Type == util.Paging {
		total, err := accountService.Count(c)
		if err != nil {
			appG.Response(constant.ERROR_COUNT_ACCOUNT_FAIL, nil)
			return
		}
		data["total"] = total
	}
	appG.Response(constant.SUCCESS, data)
}
