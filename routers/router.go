package routers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/LamTNB/go-template/middlewares/jwt"
	"gitlab.com/LamTNB/go-template/routers/api"
	"gitlab.com/LamTNB/go-template/routers/api/v1"
)

// InitRouter initialize routing information
func InitRouter() *gin.Engine {
	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())

	r.POST("/auth", api.GetAuth)
	apiv1 := r.Group("/api/v1")
	apiv1.GET("/hello", v1.SayHello)

	apiv1.Use(jwt.JWT())
	{
		apiv1.GET("/account", v1.GetAccounts)
	}

	return r
}
