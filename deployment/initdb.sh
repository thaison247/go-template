#!/bin/bash
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" -d "$POSTGRES_DB"  <<-EOSQL
   create table public.account
   (
       id         bigint,
       created_at timestamp not null,
       updated_at timestamp not null,
       deleted_at timestamp,
       name       varchar(255)
   );
   INSERT INTO public.account (id, created_at, updated_at, deleted_at, name) VALUES (1, '2022-03-04 15:46:24.000000', '2022-03-04 15:46:26.000000', null, 'LTNB');
EOSQL